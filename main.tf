provider "aws" {
  alias = "src"
}

locals {
  bucket_logs_name = "${var.bucket_name}-logs"

  special_tags = {
    "Website" = "${var.bucket_name}"
  }
  special_logs_tags = {
    "LogOf_Website_Bucket" = "${var.bucket_name}"
  }
}

#######################################################
# s3 bucket WITH enabled static website and encryption
#######################################################

resource "aws_s3_bucket" "s3_website_bucket" {
  bucket        = "${var.bucket_name}"
  region        = "${var.bucket_region}"
  provider      = "aws.src"
  acl           = "public-read"
  force_destroy = "${var.bucket_force_destroy}"

  logging {
    target_bucket = "${aws_s3_bucket.s3_website_logs_bucket.id}"
    target_prefix = "log/"
  }

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = "${merge(map("Name", var.bucket_name), local.special_tags,  var.extra_tags)}"
}



resource "aws_s3_bucket" "s3_website_logs_bucket" {
  bucket        = "${local.bucket_logs_name}"
  region        = "${var.bucket_region}"
  provider      = "aws.src"
  acl           = "log-delivery-write"
  force_destroy = "${var.bucket_force_destroy}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }

  tags = "${merge(map("Name", local.bucket_logs_name), local.special_logs_tags, var.extra_tags)}"
}
